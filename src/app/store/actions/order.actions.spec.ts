import * as fromOrder from "./order.actions";

describe('Order', () => {
  it('should create an instance', () => {
    expect(fromOrder.loadOrders.type).toBe('[Order] Load Orders');
  });
});
