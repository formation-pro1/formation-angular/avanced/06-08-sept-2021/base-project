import { createAction, props } from '@ngrx/store';
import {Client} from "../../core/models/client";

export const loadClients = createAction(
  '[Client] Load Clients'
);

export const loadClientsSuccess = createAction(
  '[Client] Load Clients Success',
  props<{ data: Client[] }>()
);

export const createClient = createAction(
  '[Client] Create Client',
  props<{ payload: Client }>()
);

export const createClientSuccess = createAction(
  '[Client] Create Client Success',
);

export const editClient = createAction(
  '[Client] Edit Client',
  props<{ payload: Client }>()
);

export const editClientSuccess = createAction(
  '[Client] Edit Client Success'
);

export const deleteClient = createAction(
  '[Client] Delete Client',
  props<{ id: number }>()
  // props<{ data: Client }>()
);

export const deleteClientSuccess = createAction(
  '[Client] Delete Client Success'
);

/*
export const deleteClientSuccess = createAction(
  '[Client] Delete Client Success',
  props<{ id: number }>()
);
*/

export const clientFailure = createAction(
  '[Client] Delete Client Generic Error',
  props<{ error: any }>()
);


