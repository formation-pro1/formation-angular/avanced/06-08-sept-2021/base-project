import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {ClientService} from "../../clients/services/client.service";
import * as ClientActions from "../actions/client.actions";
import {catchError, map, switchMap, tap} from "rxjs/operators";
import {Client} from "../../core/models/client";
import {of} from "rxjs";
import {Router} from "@angular/router";


@Injectable()
export class ClientEffects {

  loadClients$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ClientActions.loadClients),
        switchMap(_ => this.clientService.list()),
        map((data: Client[]) => ClientActions.loadClientsSuccess({data})),
        catchError((error) => of(ClientActions.clientFailure({error})))
      )
  );

  createClients$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ClientActions.createClient),
        switchMap(({payload}) => this.clientService.create(payload)),
        map(_ => ClientActions.createClientSuccess()),
        catchError((error) => of(ClientActions.clientFailure({error})))
      )
  );

  editClients$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ClientActions.editClient),
        switchMap(({payload}) => this.clientService.update(payload)),
        map(_ => ClientActions.editClientSuccess()),
        catchError((error) => of(ClientActions.clientFailure({error})))
      )
  );

  deleteClients$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ClientActions.deleteClient),
        switchMap(({id}) => this.clientService.delete(id)),
        // switchMap(({id}) => this.clientService.delete(id).pipe(map( _ => id))),
        map(_ => ClientActions.deleteClientSuccess()),
        // map(id => ClientActions.deleteClientSuccess({id})),
        catchError((error) => of(ClientActions.clientFailure({error})))
      )
  );

  /** Side effect on the createClientSuccess => redirect to client route
   * {dispatch: false} => break the infinite loop
   */
  clientSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ClientActions.createClientSuccess, ClientActions.editClientSuccess),
        tap(() => this.router.navigate(['clients']))
      ),
    {
      dispatch: false
    });

  deleteClientsSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ClientActions.deleteClientSuccess),
        map(_ => ClientActions.loadClients()),
      ));

  constructor(
    private clientService: ClientService,
    private router: Router,
    private actions$: Actions
  ) {
  }

}
