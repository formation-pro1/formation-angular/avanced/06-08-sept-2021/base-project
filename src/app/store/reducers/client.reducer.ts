import { createReducer, on } from '@ngrx/store';
import * as ClientsActions from '../actions/client.actions';
import {Client} from "../../core/models/client";

export const clientFeatureKey = 'client';

export interface State {
  data: Client[];
  loaded: boolean;
  loading: boolean;
}

export const initialState: State = {
  data: [],
  loaded: false,
  loading: false
};

export const reducer = createReducer(
  initialState,
  on(ClientsActions.loadClients, state => ({ ...state, loading: true })),
  on(ClientsActions.loadClientsSuccess, (state, { data }) => ({ ...state, data, loading: false, loaded: true})),

  on(ClientsActions.createClient, state => ({ ...state, loading: true })),
  on(ClientsActions.createClientSuccess, state => ({...state, loading: false, loaded: true})),

  on(ClientsActions.editClient, state => ({ ...state, loading: true })),
  on(ClientsActions.editClientSuccess, state => ({...state, loading: false, loaded: true})),
  
  on(ClientsActions.deleteClient, state => ({ ...state, loading: true })),
  on(ClientsActions.deleteClientSuccess, state => ({...state, loading: false, loaded: true})),
  /*
    on(ClientsActions.deleteClientSuccess, (state, {id}) => ({
      ...state, loading: false, loaded: true, data: state.data.filter(client => id !== client.id)
    })),
   */
  on(ClientsActions.clientFailure, (state, {error}) => {
    console.error(error);
    return {...state, loading: false};
  }),
);

