import {ComponentFixture, TestBed} from '@angular/core/testing';

import {BtnComponent} from './btn.component';
import {RouterTestingModule} from "@angular/router/testing";
import {DebugElement} from "@angular/core";
import {By} from "@angular/platform-browser";

describe('BtnComponent', () => {
  let component: BtnComponent;
  let fixture: ComponentFixture<BtnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BtnComponent],
      imports: [
        RouterTestingModule
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Template test', () => {
    test('should have a label', () => {
      component.label = 'label1'
      fixture.detectChanges();
      // Récupération de l’élément de debug a partir de la fixture
      const deComp: DebugElement = fixture.debugElement;
      const e: HTMLElement = deComp.query(By.css('button')).nativeElement;
      // @ts-ignore
      expect(e.textContent.trim()).toStrictEqual('label1');
    });
  });
});
