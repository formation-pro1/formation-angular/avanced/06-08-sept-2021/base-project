import { of } from "rxjs";

export const authServiceMock = {
  isLogged$: of(true),
  user$: of({login: 'login'})
}
