import {TestBed, waitForAsync} from '@angular/core/testing';

import {ClientService} from './client.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {Client} from "../../core/models/client";

const client1: Client = {
  email: "email", id: 0, name: "name", state: "Activate"
}

describe('ClientService', () => {
  let service: ClientService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(ClientService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(service).toBeTruthy();
    });
  });

  describe('Typescript test', () => {
    describe('List Client', () => {
      test('should get list', waitForAsync(() => {
        service.list().subscribe(list => {
          expect(list).toEqual([])
        });

        const req = httpTestingController.expectOne(`http://localhost:3000/clients`);
        expect(req.request.method).toEqual('GET');

        req.flush([])
      }));
      test('should get list', (done) => {
        service.list().subscribe(list => {
          expect(list).toEqual(['test'])
          done();
        });

        const req = httpTestingController.expectOne(`http://localhost:3000/clients`);
        expect(req.request.method).toEqual('GET');

        req.flush(['test'])
      });
    });
    describe('Update Client', () => {
      test('should call update endpoint', waitForAsync(() => {
        service.update(client1).subscribe(client => {
          expect(client).toEqual('client1')
        });

        const req = httpTestingController.expectOne(`http://localhost:3000/clients/0`);
        expect(req.request.method).toEqual('PUT');
        expect(req.request.body).toStrictEqual(client1);

        req.flush('client1')
      }));
    });
    describe('Get Client', () => {
      test('should call get by id endpoint', waitForAsync(() => {
        service.get(1).subscribe(client => expect(client).toEqual('client1'));

        const req = httpTestingController.expectOne(`http://localhost:3000/clients/1`);
        expect(req.request.method).toEqual('GET');

        req.flush('client1')
      }));
    });
    describe('Delete Client', () => {
      test('should call delete endpoint', waitForAsync(() => {
        service.delete(client1.id).subscribe((response) =>
          expect(response.status).toEqual(204)
        );
        // const req = httpTestingController.expectOne((request: HttpRequest<any>) =>
        //   request.method === 'DELETE' && request.url === `http://localhost:3000/clients/${client1.id}`
        // );
        const req = httpTestingController.expectOne(`http://localhost:3000/clients/${client1.id}`);
        expect(req.request.method).toEqual('DELETE');

        req.flush('client1', {
          status: 204, statusText: "no content"
        })
      }));
    });
  });
});
