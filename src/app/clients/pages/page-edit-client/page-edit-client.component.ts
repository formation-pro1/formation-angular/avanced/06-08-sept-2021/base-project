import {Component, OnInit} from '@angular/core';
import {ClientService} from '../../services/client.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Client} from "../../../core/models/client";
import {Observable} from "rxjs";
import {AppState} from "../../../store";
import {Store} from "@ngrx/store";
import * as ClientsActions from "../../../store/actions/client.actions";

@Component({
  selector: 'app-page-edit-client',
  templateUrl: './page-edit-client.component.html',
  styleUrls: ['./page-edit-client.component.scss']
})
export class PageEditClientComponent implements OnInit {
  public client$!: Observable<Client>;

  constructor(
    private store: Store<AppState>,
    private clientService: ClientService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    // use action NgRx ?
    this.client$ = this.clientService.get(id);
  }

  editClient(payload: Client) {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.store.dispatch(ClientsActions.editClient({ payload: {...payload, id}}));
  }
}
