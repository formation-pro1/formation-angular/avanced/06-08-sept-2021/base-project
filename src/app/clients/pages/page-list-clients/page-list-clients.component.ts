import {Component, OnInit} from '@angular/core';
import {ClientService} from '../../services/client.service';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AppState} from '../../../store';
import {select, Store} from '@ngrx/store';
import * as ClientsActions from '../../../store/actions/client.actions';
import * as fromClient from '../../../store/reducers/client.reducer';
import {ClientState} from "../../../core/enums/state-client";
import {Client} from "../../../core/models/client";
import {selectClientList, selectClientLoading} from "../../../store/selectors/client.selectors";

@Component({
  selector: 'app-page-list-clients',
  templateUrl: './page-list-clients.component.html',
  styleUrls: ['./page-list-clients.component.scss']
})
export class PageListClientsComponent implements OnInit {
  headers = ['Actions', 'Name', 'Email', 'State'];
  clientsStates = Object.values(ClientState);

  public clients$: Observable<Client[]>;
  public loading$: Observable<boolean>;

  constructor(
    private store: Store<AppState>,
    private clientService: ClientService,
    private router: Router,
    private translateService: TranslateService
  ) {
    this.clients$ = store.pipe(select(selectClientList));
    this.loading$ = store.pipe(select(selectClientLoading));
  }

  ngOnInit(): void {
    this.store.dispatch(ClientsActions.loadClients());
  }

  update(state: string, client: Client) {
    this.clientService.update({...client, state});
  }

  change(lang: string) {
    this.translateService.use(lang);
  }

  public navigateToEdit(id: number): void {
    this.router.navigate(['clients', 'edit', id]);
  }

  delete(id: number) {
    this.store.dispatch(ClientsActions.deleteClient({id}));
  }
}
