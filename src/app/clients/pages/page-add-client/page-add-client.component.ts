import { Component } from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../store";
import {Client} from "../../../core/models/client";
import {createClient} from "../../../store/actions/client.actions";

@Component({
  selector: 'app-page-add-client',
  templateUrl: './page-add-client.component.html',
  styleUrls: ['./page-add-client.component.scss']
})
export class PageAddClientComponent {

  constructor(
    private store: Store<AppState>
  ) { }

  addClient(payload: Client) {
    this.store.dispatch(createClient({ payload }));
  }
}
