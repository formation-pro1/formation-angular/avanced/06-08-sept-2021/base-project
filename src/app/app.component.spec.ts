import {TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {NgxTranslateTestingModule} from "../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {MockHeaderComponent} from "./core/components/__mocks__/header.component";
import {MockFooterComponent} from "./core/components/__mocks__/footer.component";
import {MockIconNavComponent} from "./icons/components/__mocks__/icon-nav.component";
import {MockIconCloseComponent} from "./icons/components/__mocks__/icon-close.component";
import {MockNavComponent} from "./core/components/__mocks__/nav.component";
import {MockUiComponent} from "./ui/components/ui/__mocks__/ui.component";
import {translateServiceMock} from "../../__mocks__/@ngx-translate/core/translate.service.mock";

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NgxTranslateTestingModule,
        RouterTestingModule
      ],
      declarations: [
        AppComponent,
        MockUiComponent,
        MockNavComponent,
        MockIconNavComponent,
        MockIconCloseComponent,
        MockHeaderComponent,
        MockFooterComponent
      ],
    }).compileComponents();
  });

  describe('Init test', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    test('should create the app', () => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.componentInstance;
      expect(app).toBeTruthy();
    });
    test('should init the translate lang from getBrowserLang', () => {
      TestBed.createComponent(AppComponent);
      expect(translateServiceMock.addLangs).toHaveBeenNthCalledWith(1, ['en', 'fr'])
      expect(translateServiceMock.setDefaultLang).toHaveBeenNthCalledWith(1, 'en')
      expect(translateServiceMock.getBrowserLang).toBeCalledTimes(1)
      expect(translateServiceMock.use).toHaveBeenNthCalledWith(1, 'fr')
    });
    test('should init the translate lang with getBrowserLang not managed', () => {
      translateServiceMock.getBrowserLang.mockImplementationOnce(() => 'bg')

      TestBed.createComponent(AppComponent);
      expect(translateServiceMock.addLangs).toHaveBeenNthCalledWith(1, ['en', 'fr'])
      expect(translateServiceMock.setDefaultLang).toHaveBeenNthCalledWith(1, 'en')
      expect(translateServiceMock.getBrowserLang).toBeCalledTimes(1)
      expect(translateServiceMock.use).toHaveBeenNthCalledWith(1, 'en')
    });
  });

  test(`should have as title 'angular-formation'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('Voici la formation du lundi 28');
  });

  test('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('Voici la formation du lundi 28');
  });
});
