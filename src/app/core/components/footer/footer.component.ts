import {Component} from '@angular/core';
import {VersionService} from '../../services/version.service';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {

  public version!: number;

  constructor(
    private versionService: VersionService,
    public translateService: TranslateService
  ) {
    this.versionService.numVersion.subscribe((next) => this.version = next);
  }

  use(value: string) {
    this.translateService.use(value)
  }

  getLangs(): string[] {
    return this.translateService.getLangs()
  }

  currentLang(): string {
    return this.translateService.currentLang
  }
}
