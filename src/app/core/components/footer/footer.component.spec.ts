import {ComponentFixture, TestBed} from '@angular/core/testing';

import {FooterComponent} from './footer.component';
import {NgxTranslateTestingModule} from "../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {VersionService} from "../../services/version.service";
import {versionServiceMock} from "../../services/__mocks__/version.service";
import {translateServiceMock} from "../../../../../__mocks__/@ngx-translate/core/translate.service.mock";

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FooterComponent],
      imports: [
        NgxTranslateTestingModule
      ],
      providers: [
        {provide: VersionService, useValue: versionServiceMock}
      ]
    })
      .compileComponents();
  });

  afterEach(() => jest.clearAllMocks());

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should create', () => {
      expect(component).toBeTruthy();
      expect(component.version).toEqual(2);
    });
  });

  describe('Typescript test', () => {
    test('should call translateService#use', () => {
      component.use('toto');
      expect(translateServiceMock.use).toHaveBeenNthCalledWith(1, 'toto')
    });
  });
});
