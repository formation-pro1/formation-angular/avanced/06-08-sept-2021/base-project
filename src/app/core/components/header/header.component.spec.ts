import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {HeaderComponent} from './header.component';
import {authServiceMock} from "../../../shared/services/__mocks__/auth.service";
import {AuthService} from "../../../shared/services/auth.service";
import {NgxTranslateTestingModule} from "../../../../../__mocks__/@ngx-translate/core/ngx-translate-testing.module";
import {DebugElement} from "@angular/core";
import {By} from "@angular/platform-browser";
import {of} from "rxjs";
import {RouterTestingModule} from "@angular/router/testing";

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      imports: [
        RouterTestingModule,
        NgxTranslateTestingModule
      ],
      providers: [
        {provide: AuthService, useValue: authServiceMock}
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Init test', () => {
    test('should be created', () => {
      expect(component).toBeTruthy();
    });
  });
  describe('Template test', () => {
    test('should display title', () => {
      const deComp: DebugElement = fixture.debugElement;
      const e: HTMLElement = deComp.query(By.css('p')).nativeElement;
      // @ts-ignore
      expect(e.textContent.trim()).toStrictEqual('[translated]HEADER.TITLE');
    });
    test('should display welcome message on connected user', waitForAsync(() => {
      component.userConnected$.subscribe(userConnected => expect(userConnected).toBeTruthy())
      const deComp: DebugElement = fixture.debugElement;
      const e: HTMLElement = deComp.query(By.css('span')).nativeElement;
      // @ts-ignore
      expect(e.textContent.trim()).toStrictEqual('[translated]HEADER.WELCOME:{\"login\":\"login\"}');
    }));
    test('should not display welcome message on disconnected user', waitForAsync(() => {
      component.userConnected$ = of(false);
      fixture.detectChanges()
      component.userConnected$.subscribe(userConnected => expect(userConnected).toBeFalsy())
      const deComp: DebugElement = fixture.debugElement;
      expect(deComp.query(By.css('span'))).toBeNull();
    }));
  });
  describe('Snapshot test', () => {
    test('user connected', () => {
      expect(fixture).toMatchSnapshot();
    });
    test('user not connected', () => {
      component.userConnected$ = of(false);
      fixture.detectChanges()
      expect(fixture).toMatchSnapshot();
    });
  });
});
