import { Component, OnInit } from '@angular/core';
import { VersionService } from '../../services/version.service';
import {AuthService} from "../../../shared/services/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public param: { login: string } = {login: ''};
  public userConnected$ = this.authService.isLogged$;

  constructor(
    private authService: AuthService
  ) {
  }

  ngOnInit(): void {
    this.authService.user$.subscribe(user => this.param = {login : user?.login ?? ''} )
  }
}
