import {of} from 'rxjs';

export const versionServiceMock = {
  numVersion: of(2)
};
