import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot, ActivatedRoute
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {Order} from "../../core/models/order";
import {OrdersService} from "../services/orders.service";
import * as ClientsActions from "../../store/actions/client.actions";
import {AppState} from "../../store";
import {Store} from "@ngrx/store";

type T = Order

@Injectable({
  providedIn: 'root'
})
export class GetOrderResolver implements Resolve<T> {
  constructor(
    private ordersService: OrdersService
  ) {
  }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<T> {
    // TODO check better solution
    return this.ordersService.getOrderById(Number(route.paramMap.get('id')));
  }
}
