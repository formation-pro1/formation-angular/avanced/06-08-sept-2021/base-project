import { TestBed } from '@angular/core/testing';

import { GetOrderResolver } from './get-order.resolver';

describe('GetOrderResolver', () => {
  let resolver: GetOrderResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(GetOrderResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
