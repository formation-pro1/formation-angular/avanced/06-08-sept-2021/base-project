import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { StateOrder } from 'src/app/core/enums/state-order';
import { Order } from 'src/app/core/models/order';
import { OrdersService } from '../../services/orders.service';

@Component({
  selector: 'app-page-list-orders',
  templateUrl: './page-list-orders.component.html',
  styleUrls: ['./page-list-orders.component.scss']
})
export class PageListOrdersComponent implements OnInit {

  public ordersCollection$!: Observable<Order[]>;
  public possibleStates = Object.values(StateOrder);

  public headers = [
    'Action',
    'Type',
    'Client',
    'NbJours',
    'Tjm HT',
    'Total HT',
    'Total TTC',
    'State',
  ];

  public myTitle = 'Liste des commandes';

  constructor(private ordersService: OrdersService,
    private router: Router) {

      
    console.log(this.possibleStates);
    this.ordersCollection$ = this.ordersService.orders
    .pipe(
      map((tableau) => {
        return tableau.map((element) => {
          return new Order(element);
        })
      })
    );
  }

  ngOnInit(): void {}

  public changeTitle(): void {
    this.myTitle = 'Liste des commandes ' + Math.random();
  }

  public changeState(item: Order, selectEvent: any): void {
    this.ordersService.changeState(item, selectEvent.target.value).subscribe(
      (res) => {
        console.log(res);
      }
    );
  }

  public navigateToEdit(id: number): void {
    this.router.navigate(['orders', 'edit', id]);
  }
}
