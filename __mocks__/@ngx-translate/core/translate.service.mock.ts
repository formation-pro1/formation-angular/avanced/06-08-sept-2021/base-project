import {of} from 'rxjs';
import fn = jest.fn;

export const translateServiceMock  = {
  get: fn(() => of('key')),
  setDefaultLang: fn((arg) => {}),
  use: fn((arg) => {}),
  instant: fn((arg) => arg),
  addLangs: fn((arg) => {}),
  getLangs: fn((arg) => ['en', 'fr']),
  getBrowserLang: fn((arg) => 'fr'),
};
